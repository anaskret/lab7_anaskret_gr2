﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab7
{
    class Company : Account
    {
        public string CompanyName { get; set; }

        public Company(string firstName, string lastName, string phoneNumber, string address, string companyName) : base(firstName, lastName, phoneNumber, address)
        {
            CompanyName = companyName;
        }

        public override string ToString()
        {
            return $"{base.ToString()}, Company Name: {CompanyName}";
        }
    }
}
