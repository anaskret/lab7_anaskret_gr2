﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Lab7
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader r = new StreamReader("database.json");
            string json = r.ReadToEnd();
            var app = JsonConvert.DeserializeObject<ReadDatabase>(json);
            r.Close();
            r.Dispose();
        }
    }
}
