﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab7
{
    class Account
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }

        public Account(string firstName, string lastName, string phoneNumber, string address)
        {
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
            Address = address;
        }

        public override string ToString()
        {
            return $"First Name: {FirstName}, Last Name: {LastName}, Phone Number: {PhoneNumber}, Address: {Address}";
        }
    }
}
